//
//  IonixTests.swift
//  IonixTests
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright © 2020 Alejandro Rodriguez. All rights reserved.
//

import XCTest
@testable import Ionix

class IonixTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
  // MARK: - MODEL
    func test_model_callmenu() {
        let expectation = self.expectation(description: "get menu list")

        let service = MainService()
        let model = MainModel(service)
        model.callMenus { status in
            XCTAssert(status == .ok)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 100, handler: nil)
    }

    func test_model_callProfile() {
        let expectation = self.expectation(description: "get profile list")

        let service = MainService()
        let model = MainModel(service)

        model.callProfile(rut: "1-9") { status in
            switch status {
            case .error:
                XCTFail()
            case .empty:
                XCTFail()
            case .ok:
                if let profile = model.profile {
                    XCTAssert(profile.detail.email == "asmith@gmail.com")
                } else {
                    XCTFail()
                }
            }
            expectation.fulfill()
        }

        waitForExpectations(timeout: 10, handler: nil)
    }
    
    func test_model_callProfile_fail() {
        let expectation = self.expectation(description: "get profile Fail")
        
        let service = MainService()
        let model = MainModel(service)
        
        model.callProfile(rut: ":(") { status in
            XCTAssert(status == .error)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 10, handler: nil)
    }
    
    // MARK: - PRESENTER

}

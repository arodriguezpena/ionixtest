//
//  AppDelegate.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright © 2020 Alejandro Rodriguez. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        self.window = UIWindow(frame: UIScreen.main.bounds)

        self.window?.rootViewController = MainFactory().getMainViewController()
        self.window?.makeKeyAndVisible()

        return true
    }

}

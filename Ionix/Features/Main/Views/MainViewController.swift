//
//  MainViewController.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright (c) 2020 All rights reserved.

import UIKit
protocol MainDisplayLogic: class {
    func loadData(service: FromService)
    func errorData(service: FromService)
    func empyData(service: FromService)
}

class MainViewController: UIViewController, MainDisplayLogic {
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var collection: MenuCollection!
    var presenter: MainPresentationLogic!

    override public func viewDidLoad() {
		super.viewDidLoad()
        collection.delegate = self
        self.presenter.callMenu()
	}

    func showAlertWithTextField() {
        let alertController = UIAlertController(title: MainConfig.View.title, message: MainConfig.View.needValidateRut, preferredStyle: .alert)

        alertController.addTextField { textField in
            textField.placeholder = MainConfig.View.needRutPlaceHolder
        }

        let confirmAction = UIAlertAction(title: MainConfig.View.accept, style: .default) { [weak alertController] _ in
            guard let alertController = alertController, let textField = alertController.textFields?.first else { return }
            self.presenter.callProfile(rut: textField.text ?? "")
        }

        alertController.addAction(confirmAction)
        let cancelAction = UIAlertAction(title: MainConfig.View.cancel, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func hideLoading() {
        self.loadingIndicator.stopAnimating()
        self.loadingView.isHidden = true
    }
}

extension MainViewController: MenuCollectionDelegate {
    func tapFirstItem() {
        if InternetStatus.isConnected() {
             showAlertWithTextField()
        } else {
            let alert = UIAlertController(title: MainConfig.View.title, message: MainConfig.View.errorInternet, preferredStyle: .alert)
            let btnAccept = UIAlertAction(title: MainConfig.View.accept, style: .default)
            alert.addAction(btnAccept)
            self.present(alert, animated: true)
        }
    }
}

extension MainViewController {
    func loadData(service: FromService) {

        switch service {
        case .menu:
            DispatchQueue.main.async {
                self.hideLoading()
                self.collection.setupDataSource(self.presenter.dataStore.listOfMenus)
            }
        case .profile:
            guard let profile = self.presenter.dataStore.profile else {
                self.errorData(service: .profile)
                return
            }

            let alert = UIAlertController(title: MainConfig.View.title, message: "\(profile.detail.email) - \(profile.detail.phone_number)", preferredStyle: .alert)
            let btnAccept = UIAlertAction(title: MainConfig.View.accept, style: .default)
            alert.addAction(btnAccept)
            self.present(alert, animated: true)
        }
    }

    func errorData(service: FromService) {
        let alert = UIAlertController(title: MainConfig.View.title, message: MainConfig.View.errorGeneric, preferredStyle: .alert)
        let btnAccept = UIAlertAction(title: MainConfig.View.accept, style: .default)
        let btnRetry = UIAlertAction(title: MainConfig.View.retry, style: .default) { (_) in
            switch service {
            case .menu:
                self.presenter.callMenu()
            case .profile:
                 self.tapFirstItem()
            }
        }
        
        alert.addAction(btnAccept)
        alert.addAction(btnRetry)
        self.present(alert, animated: true)
    }

    func empyData(service: FromService) {
        let alert = UIAlertController(title: MainConfig.View.title, message: MainConfig.View.errorMenu, preferredStyle: .alert)
        let btnAccept = UIAlertAction(title:MainConfig.View.accept, style: .default)
        alert.addAction(btnAccept)
        self.present(alert, animated: true)
    }
}

//
//  MenuCollectionCell.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/30/20.
//  Copyright © 2020 Alejandro Rodriguez. All rights reserved.
//

import Foundation
import UIKit
import FontAwesome_swift
final class MenuCollectionViewCell: UICollectionViewCell {

    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.init(hexString: "#CDD2D6", alpha: 0.95)
        view.layer.cornerRadius = 8
        return view
    }()

    let iconImage: UIImageView = {
        let image = UIImageView()
        return image
    }()

    let titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor.init(hexString: "#45474B")
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12)

        return label
    }()
    func setup(_ menu: MenuEntity ) {
        backgroundColor = .clear
        addSubview(containerView)
        self.addContraintsWithFormat(format: "H:|-0-[v0]-0-|", views: containerView)
        self.addContraintsWithFormat(format: "V:|-0-[v0]-0-|", views: containerView)

        containerView.addSubview(iconImage)
        containerView.addSubview(titleLabel)

        containerView.addContraintsWithFormat(format: "H:|-0-[v0]-0-|", views: titleLabel)

        let width = 50
        containerView.addContraintsWithFormat(format: "V:|-10-[v0(\(width))]-0-[v1]-10-|", views: iconImage, titleLabel)

        let xConstraint = NSLayoutConstraint(item: iconImage, attribute: .centerX, relatedBy: .equal, toItem: self.containerView, attribute: .centerX, multiplier: 1, constant: 0)

        containerView.addConstraint(xConstraint)

        titleLabel.text = menu.title
        titleLabel.sizeToFit()

        if let icon = FontAwesome(rawValue: "fa-\(menu.icon)") {
            iconImage.image = UIImage.fontAwesomeIcon(name: icon, style: .solid, textColor: UIColor.init(hexString: "#45474B"), size: CGSize(width: width, height: width))
        } else {
            iconImage.image = UIImage.fontAwesomeIcon(name: .star, style: .solid, textColor: UIColor.init(hexString: "#45474B"), size: CGSize(width: width, height: width))
        }

    }
}

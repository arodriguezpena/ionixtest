//
//  MenuCollection.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright © 2020 Alejandro Rodriguez. All rights reserved.
//

import Foundation
import UIKit
protocol MenuCollectionDelegate: class {
    func tapFirstItem()
}
class MenuCollection: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    weak var delegate: MenuCollectionDelegate?
    private var menuList: [MenuEntity] = []

    func setupDataSource(_ menu: [[MenuEntity]]) {
        for p in menu {
            for x in p {
                menuList.append(x)
            }
        }
        self.collectionView.reloadData()
    }

    let flowLayout: UICollectionViewFlowLayout = {
        let f = UICollectionViewFlowLayout()
        f.scrollDirection = .vertical
//        f.minimumLineSpacing = 0
        f.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        return f
    }()

    private lazy var collectionView: UICollectionView = {
        let cv = UICollectionView(frame: CGRect.zero, collectionViewLayout: flowLayout)
        cv.delegate = self
        cv.backgroundColor = .clear
        cv.dataSource = self
        cv.showsHorizontalScrollIndicator = false
        cv.showsVerticalScrollIndicator = false
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(MenuCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        return cv
    }()

    func setup() {
        self.addSubview(collectionView)
        self.addContraintsWithFormat(format: "H:|-0-[v0]-0-|", views: collectionView)
        self.addContraintsWithFormat(format: "V:|-0-[v0]-0-|", views: collectionView)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as? MenuCollectionViewCell else {
           fatalError("No se encontro la celda")
        }
        cell.setup(menuList[indexPath.item])
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.bounds.width / 3) - 7
        return CGSize(width: width, height: width)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item == 0 {
            self.delegate?.tapFirstItem()
        }
    }

}

//
//  MainConfig.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright © 2020 Alejandro Rodriguez. All rights reserved.
//

import Foundation

enum MainConfig {
    enum Services {
        static let Menus = "https://api.myjson.com/bins/h3ayu"
        static let Profile = "https://sandbox.ionix.cl/test-tecnico/search?rut="
    }
    enum View {
        static let title = "Mi Copiloto"
        static let needValidateRut = "Necesitamos validar tu Rut"
        static let needRutPlaceHolder = "Ingresa tu rut"
        static let accept = "Aceptar"
        static let cancel = "Cancelar"
        static let retry = "Reintentar"
        static let errorMenu = "Lo sentimos, no pudimos cargar el menú"
        static let errorGeneric = "Lo sentimos, ocurrio un error"
        static let errorInternet = "Al parecer no tienes internet disponible, vuelve a intentar más tarde"
    }
    
}

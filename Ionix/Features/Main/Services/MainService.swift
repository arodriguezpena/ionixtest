//
//  MainService.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright (c) 2020  All rights reserved.

import UIKit
import Alamofire
protocol MainServiceLogic: class {
    func callAPI(callback: @escaping ([[MenuEntity]]?) -> Void)
    func callProfile(rut: String, callback: @escaping (ResponseProfileEntity?) -> Void)
}

class MainService: MainServiceLogic {

    func callAPI(callback: @escaping ([[MenuEntity]]?) -> Void) {

        guard let uri = URL(string: MainConfig.Services.Menus) else {
            return callback(nil)
        }

        AF.request(uri).response { (response) in

            guard let data = response.data else {
                callback(nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let collection = try decoder.decode([[MenuEntity]].self, from: data)
                callback(collection)
            } catch let error {
                print(error)
                callback(nil)
            }
        }
    }

    func callProfile(rut: String, callback: @escaping (ResponseProfileEntity?) -> Void) {

        guard let rutEncript = Encript().encriptDES(rut) else {
            return callback(nil)
        }
        guard let uri = URL(string: "\(MainConfig.Services.Profile)\(rutEncript)") else {
            return callback(nil)
        }

        AF.request(uri).response { (response) in

            guard let data = response.data else {
                callback(nil)
                return
            }
            do {
                let decoder = JSONDecoder()
                let collection = try decoder.decode(ResponseProfileEntity.self, from: data)
                callback(collection)
            } catch let error {
                print(error)
                callback(nil)
            }
        }
    }
}

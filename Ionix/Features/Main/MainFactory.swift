//
//  MainFactory.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright (c) 2020 All rights reserved.

import Foundation
import UIKit
class MainFactory {
    func getMainViewController() -> UIViewController {
        let service = MainService()
        let model = MainModel(service)
        let presenter = MainPresenter(model)
        let viewController = MainViewController()
        viewController.presenter = presenter
        presenter.view = viewController
        model.service = service
        return viewController
    }
}

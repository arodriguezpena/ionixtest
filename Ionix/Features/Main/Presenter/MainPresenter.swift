//
//  MainPresenter.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright (c) 2020 All rights reserved.

import Foundation

enum FromService {
    case menu
    case profile
}
protocol MainPresentationLogic {
    var dataStore: MainDataStore { get set }
    func callMenu()
    func callProfile(rut: String)
}

class MainPresenter: MainPresentationLogic {
	weak var view: MainDisplayLogic?
	var model: MainModelLogic
	var dataStore: MainDataStore

	init(_ model: MainModel) {
		self.model = model
		self.dataStore = model
	}

    func callMenu() {
        self.model.callMenus { [weak self] status in
            guard let self = self, let view = self.view else {
                return
            }

            switch status {
            case .ok:
                view.loadData(service: .menu)
            case .empty:
                view.empyData(service: .menu)
            case .error:
                view.errorData(service: .menu)
            }
        }
    }

    func callProfile(rut: String) {
        self.model.callProfile(rut: rut) { [weak self] status in
            guard let self = self, let view = self.view else {
                return
            }
            switch status {
            case .ok:
                view.loadData(service: .profile)
            case .empty:
                view.empyData(service: .profile)
            case .error:
                view.errorData(service: .profile)
            }
        }
    }
}

//
//  Entities.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright © 2020 Alejandro Rodriguez. All rights reserved.
//

import Foundation

struct MenuEntity: Codable {
    let title: String
    let icon: String
}

struct ResponseProfileEntity: Codable {
    let responseCode: Int
    let description: String
    let result: ProfileItemsEntity
}
struct ProfileItemsEntity: Codable {
    let items: [ProfileEntity]
}
struct ProfileEntity: Codable {
    let name: String
    let detail: ProfileDetailEntity
}

struct ProfileDetailEntity: Codable {
    let email: String
    let phone_number: String
}

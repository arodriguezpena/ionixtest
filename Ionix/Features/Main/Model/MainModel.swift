//
//  MainModel.swift
//  Ionix
//
//  Created by Alejandro  Rodriguez on 1/29/20.
//  Copyright (c) 2020  All rights reserved.

import UIKit

enum StatusService {
    case ok
    case error
    case empty
}

protocol MainModelLogic {
    func callMenus(callback: @escaping (StatusService) -> Void)
    func callProfile(rut: String, callback: @escaping (StatusService) -> Void)
}

protocol MainDataStore {
	var listOfMenus: [[MenuEntity]] { get set }
    var profile: ProfileEntity? { get set }
}

class MainModel: MainModelLogic, MainDataStore {
    let ROW_PROFILE = 1

	var service: MainService
    var listOfMenus: [[MenuEntity]] = []
    var profile: ProfileEntity?

	init(_ service: MainService) {
		self.service = service
	}

    func callMenus(callback: @escaping (StatusService) -> Void) {
        self.service.callAPI { [weak self] listOfMenus in
            guard let list = listOfMenus, let self = self else {
                return callback(.error)
            }
            if list.count == 0 {
                return callback(.empty)
            } else {
                self.listOfMenus = list
                return callback(.ok)
            }
        }
    }

    func callProfile(rut: String, callback: @escaping (StatusService) -> Void) {
        self.service.callProfile(rut: rut) { [weak self] response in
            guard let rProfile = response, let self = self else {
                return callback(.error)
            }

            if rProfile.result.items.count == 0 {
                return callback(.error)
            }

            if rProfile.result.items.count < self.ROW_PROFILE {
                return callback(.error)
            }

            self.profile = rProfile.result.items[self.ROW_PROFILE]
            callback(.ok)
        }
    }
}
